#ifndef ULM_ALU_H
#define ULM_ALU_H

#include <stdbool.h>
#include <stdint.h>

enum {
    ZF = 0,
    CF,
    SF,
    OF,
    NUM_STATUS_FLAGS,
};
extern bool statusReg[NUM_STATUS_FLAGS];

typedef uint8_t Reg;

/*
 * Exept for the zero register regBytes returns a pointer to the
 * least significant byte of register reg.
 * For the zero register it returns a null pointer.
 **/
uint64_t *regDevice(Reg reg);

uint64_t regVal(Reg reg);
void setReg(uint64_t val, Reg reg);

// compute b + a
void add64(uint64_t a, uint64_t b, Reg dest);

// compute b - a
void sub64(uint64_t a, uint64_t b, Reg dest);

// compute b * a
void mul64(uint64_t a, uint64_t b, Reg dest);
void mul128(uint64_t a, uint64_t b, Reg dest);

// compute b / a and b % a
void div128(uint64_t a, uint64_t b0, uint64_t b1, Reg dest);
void idiv64(int64_t a, int64_t b, Reg dest);

// compute b * 2^a mod 2^64
void shiftLeft64(uint64_t a, uint64_t b, Reg dest);

// compute b / 2^a mod 2^64
void shiftRightSigned64(uint64_t a, int64_t b, Reg dest);
void shiftRightUnsigned64(uint64_t a, uint64_t b, Reg dest);

void and64(uint64_t a, uint64_t b, Reg dest);
void or64(uint64_t a, uint64_t b, Reg dest);
void not64(uint64_t a, Reg dest);

void printALU(uint8_t firstReg, uint8_t lastReg);

#endif // ULM_ALU_H
