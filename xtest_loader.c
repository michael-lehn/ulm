#include <stdio.h>
#include <stdlib.h>

#include "loader.h"
#include "vmem.h"

int
main(int argc, const char **argv)
{
    if (argc != 2) {
	fprintf(stderr, "usage: %s prog\n", argv[0]);
	exit(1);
    }
    load(argv[1]);
    // printf("brk = %016llx\n", brk);
    // printVMem(0, brk, 4);
    // printVMem();
}
