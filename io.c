#include <stdio.h>

#include "io.h"

void
printChar(int c)
{
    putchar(c);
}

int
readChar()
{
    return getchar();
}
