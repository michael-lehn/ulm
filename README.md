# ulm

ULM (Ulm Lecture Machine) is a 64-bit RISC architecture. Its design is mostly inspired by the [MMIX](https://en.wikipedia.org/wiki/MMIX) and [Intel 64](https://de.wikipedia.org/wiki/Intel_64) architecure. Kind of a remix of MMIX which is a remix of [MIX](https://en.wikipedia.org/wiki/MIX) :D

https://www.mathematik.uni-ulm.de/numerik/hpc/ss20/hpc0/ulm.pdf
